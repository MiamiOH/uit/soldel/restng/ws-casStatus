# CAS Status Web Service

CAS status web service returns whether user's session has already authenticaed through CAS.

### Resources:

#### GET /casStatus/v1

Get CAS status. 

If user's session has already authenticated through CAS:
```json
{
    "status": true
}
```

Otherwise:
```json
{
    "status": false
}
```

#### POST /casStatus/v1/logout

Logout from CAS.
