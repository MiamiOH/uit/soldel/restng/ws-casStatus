<?php

namespace MiamiOH\CasStatusWebService\Services;

use MiamiOH\RESTng\Service;

class CasStatusService extends Service
{
    /**
     * @var CasHandler
     */
    private $casHandler;

    public function __construct(CasHandler $casHandler = null)
    {
        if ($casHandler === null) {
            $casHandler = new CasHandler();
        }

        $this->casHandler = $casHandler;
    }

    public function getStatus()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        if (isset($options['logout']) && $options['logout'] === 'true') {
            $this->casHandler->logout();
        }

        $isAuthenticated = $this->casHandler->isAuthenticated();

        $payload = [];

        if ($isAuthenticated) {
            $payload = ['status' => true];
        } else {
            $payload = ['status' => false];
        }

        $response->setPayload($payload);

        return $response;
    }

    public function logout()
    {
        $response = $this->getResponse();

        $this->casHandler->logout();

        return $response;
    }
}
