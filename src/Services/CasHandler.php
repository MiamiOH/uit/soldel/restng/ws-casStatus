<?php
/**
 * Author: liaom
 * Date: 7/24/18
 * Time: 9:08 AM
 */

namespace MiamiOH\CasStatusWebService\Services;

use phpCAS;

class CasHandler
{
    public function __construct()
    {
        phpCAS::client(
            CAS_VERSION_2_0,
            'muidp.miamioh.edu',
            443,
            '/cas',
            false
        );
        phpCAS::setNoCasServerValidation();
    }

    public function isAuthenticated(): bool {
        return phpCAS::checkAuthentication();
    }

    public function logout(): void {
        phpCAS::logout();
    }
}