<?php

namespace MiamiOH\CasStatusWebService\Resources;

use MiamiOH\CasStatusWebService\Services\CasHandler;
use MiamiOH\RESTng\App;

class CasStatusResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    /**
     * @var string
     */
    private $serviceName = 'CasStatus';
    /**
     * @var string
     */
    private $tag = "casStatus";
    /**
     * @var string
     */
    private $resourceRoot = "CasStatus.v1";
    /**
     * @var string
     */
    private $patternRoot = "/casStatus/v1";
    /**
     * @var string
     */
    private $classPath = 'MiamiOH\CasStatusWebService\Services\CasStatusService';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'CAS Status Web Service'
        ));

        $this->addDefinition([
            'name' => $this->resourceRoot,
            'type' => 'object',
            'properties' => [
                'status' => [
                    'type' => 'boolean',
                    'description' => 'whether CAS session exists'
                ]
            ]
        ]);
    }

    public function registerServices(): void
    {

        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'CAS Status Web Service'
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->resourceRoot . '.getStatus',
                'description' => 'Get CAS Status',
                'summary' => 'Get CAS Status',
                'pattern' => $this->patternRoot,
                'service' => $this->serviceName,
                'method' => 'getStatus',
                'tags' => [$this->tag],
                'returnType' => 'model',
                'options' => [
                    'logout' => [
                        'type' => 'single',
                        'enum' => [
                          'true',
                          'false'
                        ],
                        'default' => 'false',
                        'description' => 'whether need to logout first'
                    ],
                    'ticket' => [
                        'type' => 'single',
                        'description' => 'CAS ticket'
                    ]
                ],
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of classrooms',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot,
                        )
                    )
                )
            )
        );

        $this->addResource(array(
                'action' => 'create',
                'name' => $this->resourceRoot . '.logout',
                'description' => 'Logout from CAS',
                'summary' => 'Logout from CAS',
                'pattern' => $this->patternRoot . '/logout',
                'service' => $this->serviceName,
                'method' => 'logout',
                'tags' => [$this->tag],
                'returnType' => 'model',
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of classrooms',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot,
                        )
                    )
                )
            )
        );
    }

    public function registerOrmConnections(): void
    {

    }
}
