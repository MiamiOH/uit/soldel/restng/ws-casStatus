<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:07 PM
 */

namespace MiamiOH\CasStatusWebService\Tests\Feature;

use MiamiOH\CasStatusWebService\Services\CasHandler;
use MiamiOH\CasStatusWebService\Services\CasStatusService;
use MiamiOH\RESTng\Util\Request;
use PHPUnit\Framework\MockObject\MockObject;

class CasStatusTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    private $casHandler;

    /**
     * @var CasStatusService
     */
    private $casStatusService;

    /**
     * @var MockObject
     */
    private $request;

    protected function setUp()
    {
        parent::setUp();

        $this->casHandler = $this->createMock(CasHandler::class);
        $this->request = $this->createMock(Request::class);

        $this->casStatusService = new CasStatusService($this->casHandler);
        $this->casStatusService->setRequest($this->request);
    }

    public function testAuthenticated()
    {
        $this->request->method('getOptions')->willReturn([]);
        $this->casHandler->method('isAuthenticated')->willReturn(true);
        $response = $this->casStatusService->getStatus();
        $this->assertEquals([
            'status' => true
        ], $response->getPayload());
    }

    public function testUnauthenticated()
    {
        $this->request->method('getOptions')->willReturn([]);
        $this->casHandler->method('isAuthenticated')->willReturn(false);
        $response = $this->casStatusService->getStatus();
        $this->assertEquals([
            'status' => false
        ], $response->getPayload());
    }
}