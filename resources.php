<?php

return [
    'resources' => [
        'casStatus' => [
            \MiamiOH\CasStatusWebService\Resources\CasStatusResourceProvider::class
        ]
    ]
];
